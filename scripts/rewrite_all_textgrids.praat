form Rewrite TextGrids
    sentence TextGrid_folder
endform

tg_dir$ = textGrid_folder$
stringsID = Create Strings as file list: "fileList", tg_dir$+"/*.TextGrid"
nStrings = Get number of strings
for iString to nStrings
    selectObject: stringsID
    tg_name$ = Get string: iString
    tgID = Read from file: tg_dir$ + "/" +tg_name$

    #Paste your code here
    
    Save as text file: tg_dir$ + "/" +tg_name$
    removeObject: tgID
endfor

removeObject: stringsID
