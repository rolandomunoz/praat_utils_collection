tone_n1 = 0
tone_n2 = 100
clearinfo
for i to 10
  tone_fibonacci = tone_n1 + tone_n2
  appendInfoLine: tone_fibonacci
  sd = Create Sound as pure tone: "tone", 1, 0, 0.4, 44100, tone_fibonacci, 0.2, 0.01, 0.01
  Play
  tone_n1 = tone_n2
  tone_n2 = tone_fibonacci  
  removeObject: sd
endfor