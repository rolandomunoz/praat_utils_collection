# Copyright 2017 Rolando Muñoz Aramburú

tg = Merge
nTiers = Get number of tiers

for root_tier to nTiers
  root_tier$ = Get tier name: root_tier
  root_tier_isInterval = Is interval tier: root_tier

  for tier from root_tier+1 to nTiers
    tier$ = Get tier name: tier
    tier_isInterval = Is interval tier: tier
    if (root_tier$ == tier$) and (root_tier_isInterval = tier_isInterval)
      if root_tier_isInterval
        @copyIntervalTier: tier, root_tier 
      else
        @copyPointTier: tier, root_tier
      endif
      Remove tier: tier
      tier-=1
      nTiers-=1    
    endif
  endfor
endfor
    
procedure copyIntervalTier: .x, .y
  .n = Get number of intervals: .x
  for .i to .n
    .text$ = Get label of interval: .x, .i
    if .text$ <> ""
      .tmin = Get start time of interval: .x, .i
      .tmax = Get end time of interval: .x, .i
      .tmid = (.tmin+.tmax)*0.5
      .iMin_y = Get high interval at time: .y, .tmin
      .iMax_y = Get low interval at time: .y, .tmax
      .text_y$ = Get label of interval: .y, .iMin_y
      if (.iMin_y = .iMax_y) and .text_y$ == ""
        nocheck Insert boundary: .y, .tmin
        nocheck Insert boundary: .y, .tmax
        .i_y = Get interval at time: .y, .tmid
        Set interval text: .y, .i_y, .text$
      endif
    endif
  endfor
endproc

procedure copyPointTier: .x, .y
  .tg = selected("TextGrid")
  .n = Get number of points: .x
  for .i to .n
    selectObject: .tg
    .time = Get time of point: .x, .i
    .text$ = Get label of point: .x, .i
    .pointProcess = Get points: .y, "is not equal to", ""
    .isPoint = Get interval: .time
    removeObject: .pointProcess
    selectObject: .tg
    if .isPoint = undefined
      Insert point: .y, .time, .text$
    endif
  endfor
endproc
